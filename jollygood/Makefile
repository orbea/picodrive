SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

CC ?= cc
CFLAGS ?= -O1
FLAGS := -std=gnu99 -falign-functions=2
DEFS += -DEMU_F68K -D_USE_CZ80 -DNDEBUG
INCLUDES := -I$(SOURCEDIR)/..
WARNINGS := -Wall

PKG_CONFIG ?= pkg-config
CFLAGS_JG := $(shell $(PKG_CONFIG) --cflags jg)

CFLAGS_ZLIB := $(shell $(PKG_CONFIG) --cflags zlib)
LIBS_ZLIB := $(shell $(PKG_CONFIG) --libs zlib)

INCLUDES += $(CFLAGS_ZLIB)
LIBS := $(LIBS_ZLIB)
PIC := -fPIC
SHARED := $(PIC)

NAME := picodrive
PREFIX ?= /usr/local
LIBDIR ?= $(PREFIX)/lib
DATAROOTDIR ?= $(PREFIX)/share
DOCDIR ?= $(DATAROOTDIR)/doc/$(NAME)

UNAME = $(shell uname -s)
ifeq ($(UNAME), Darwin)
	SHARED += -dynamiclib
	TARGET := $(NAME).dylib
else ifeq ($(OS), Windows_NT)
	SHARED += -shared
	TARGET := $(NAME).dll
else
	SHARED += -shared
	TARGET := $(NAME).so
endif

CSRCS := cpu/cz80/cz80.c \
	cpu/drc/cmn.c \
	cpu/fame/famec.c \
	cpu/sh2/mame/sh2pico.c \
	cpu/sh2/sh2.c \
	pico/32x/32x.c \
	pico/32x/draw.c \
	pico/32x/memory.c \
	pico/32x/pwm.c \
	pico/32x/sh2soc.c \
	pico/carthw/carthw.c \
	pico/carthw/eeprom_spi.c \
	pico/carthw/svp/memory.c \
	pico/carthw/svp/ssp16.c \
	pico/carthw/svp/svp.c \
	pico/cd/cd_image.c \
	pico/cd/cdc.c \
	pico/cd/cdd.c \
	pico/cd/cue.c \
	pico/cd/gfx.c \
	pico/cd/gfx_dma.c \
	pico/cd/mcd.c \
	pico/cd/memory.c \
	pico/cd/misc.c \
	pico/cd/pcm.c \
	pico/cd/sek.c \
	pico/pico/memory.c \
	pico/pico/pico.c \
	pico/pico/xpcm.c \
	pico/sound/mix.c \
	pico/sound/sn76496.c \
	pico/sound/sound.c \
	pico/sound/ym2612.c \
	pico/cart.c \
	pico/debug.c \
	pico/draw.c \
	pico/draw2.c \
	pico/eeprom.c \
	pico/media.c \
	pico/memory.c \
	pico/misc.c \
	pico/mode4.c \
	pico/patch.c \
	pico/pico.c \
	pico/sek.c \
	pico/sms.c \
	pico/state.c \
	pico/videoport.c \
	pico/z80if.c \
	platform/common/mp3.c \
	platform/common/mp3_dummy.c \
	unzip/unzip.c \
	jg.c

# Object dirs
MKDIRS := cpu/cz80 \
	cpu/drc \
	cpu/fame \
	cpu/sh2/mame \
	pico/32x \
	pico/carthw/svp \
	pico/cd \
	pico/pico \
	pico/sound \
	platform/common \
	unzip

OBJDIR := objs

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CSRCS:.c=.o))

# Compiler command
COMPILE = $(strip $(1) $(CPPFLAGS) $(PIC) $(2) -c $< -o $@)
COMPILE_C = $(call COMPILE, $(CC) $(CFLAGS), $(1))

# Info command
COMPILE_INFO = $(info $(subst $(SOURCEDIR)/,,$(1)))

# Core commands
BUILD_JG = $(call COMPILE_C, $(FLAGS) $(WARNINGS) $(DEFS) $(INCLUDES) \
	$(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_C, $(FLAGS) $(WARNINGS) $(DEFS) $(INCLUDES))

.PHONY: all clean install install-strip uninstall

all: $(NAME)/$(TARGET)

# Core rules
$(OBJDIR)/%.o: $(SOURCEDIR)/../%.c $(OBJDIR)/.tag
	$(call COMPILE_INFO, $(BUILD_MAIN))
	@$(BUILD_MAIN)

# Shared library rules
$(OBJDIR)/%.o: $(SOURCEDIR)/%.c $(OBJDIR)/.tag
	$(call COMPILE_INFO, $(BUILD_JG))
	@$(BUILD_JG)

$(OBJDIR)/.tag:
	@mkdir -p -- $(patsubst %,$(OBJDIR)/%,$(MKDIRS))
	@touch $@

$(NAME)/$(TARGET): $(OBJS)
	@mkdir -p $(NAME)
	$(CC) $(LDFLAGS) $^ $(LIBS) $(SHARED) -o $@

clean:
	rm -rf $(OBJDIR) $(NAME)

install: all
	@mkdir -p $(DESTDIR)$(DOCDIR)
	@mkdir -p $(DESTDIR)$(LIBDIR)/jollygood
	cp $(NAME)/$(TARGET) $(DESTDIR)$(LIBDIR)/jollygood/
	cp $(SOURCEDIR)/../AUTHORS $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/../COPYING $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/README $(DESTDIR)$(DOCDIR)

install-strip: install
	strip $(DESTDIR)$(LIBDIR)/jollygood/$(TARGET)

uninstall:
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -f $(DESTDIR)$(LIBDIR)/jollygood/$(TARGET)
